package tw.idv.dumars.session.config;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.session.FlushMode;
import org.springframework.session.data.redis.config.ConfigureRedisAction;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import tw.idv.dumars.session.listener.HttpSessionMonitorListener;

import javax.servlet.http.HttpSessionListener;

@Configuration
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 7200, flushMode = FlushMode.IMMEDIATE)
@AutoConfigureBefore({ RedisAutoConfiguration.class })
@AutoConfigureAfter({ RedisStandaloneConfig.class, RedisSentinelConfig.class })
public class RedisSessionConfig {

	/**
	 * f you use @EnableRedisHttpSession, managing the SessionMessageListener and enabling
	 * the necessary Redis Keyspace events is done automatically. However, in a secured
	 * Redis environment, the config command is disabled. This means that Spring Session
	 * cannot configure Redis Keyspace events for you. To disable the automatic
	 * configuration, add ConfigureRedisAction.NO_OP as a bean.
	 * @return
	 */
	@Bean
	public static ConfigureRedisAction configureRedisAction() {
		return ConfigureRedisAction.NO_OP;
	}

	@Bean
	public HttpSessionEventPublisher httpSessionEventPublisher() {
		return new HttpSessionEventPublisher();
	}

	@Bean
	public HttpSessionListener httpSessionListener() {
		return new HttpSessionMonitorListener();
	}

}
