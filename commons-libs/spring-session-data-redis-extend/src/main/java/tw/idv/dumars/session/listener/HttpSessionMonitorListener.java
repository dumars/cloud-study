package tw.idv.dumars.session.listener;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Slf4j
public class HttpSessionMonitorListener implements HttpSessionListener {

	public void sessionCreated(HttpSessionEvent event) {
		log.trace("--------------------- create new session ------------------------");
		logging(event.getSession());
		log.trace("-----------------------------------------------------------------");
	}

	public void sessionDestroyed(HttpSessionEvent event) {
		log.trace("--------------------- destroy session ---------------------------");
		logging(event.getSession());
		log.trace("-----------------------------------------------------------------");
	}

	private void logging(HttpSession session) {
		log.trace("session id: {}", session.getId());
		log.trace("session creation time: {}", session.getCreationTime());
		log.trace("session last accessed time: {}",
				LocalDateTime.ofInstant(
						Instant.ofEpochMilli(session.getLastAccessedTime()),
						ZoneId.systemDefault()));
		log.trace("session max inactive interval: {}", session.getMaxInactiveInterval());
	}

}
