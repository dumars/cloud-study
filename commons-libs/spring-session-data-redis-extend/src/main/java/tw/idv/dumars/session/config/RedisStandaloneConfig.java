package tw.idv.dumars.session.config;

import io.lettuce.core.ClientOptions;
import io.lettuce.core.resource.ClientResources;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;

@Slf4j
@Configuration
@ConditionalOnClass({ RedisAutoConfiguration.class })
@ConditionalOnProperty("spring.redis.host")
@AutoConfigureBefore({ RedisAutoConfiguration.class })
public class RedisStandaloneConfig {

	@Autowired
	private RedisProperties properties;

	@Bean
    ClientOptions clientOptions() {
		log.debug("Initialize lettuce options disconnect behavior: reject command.");
		// @formatter:off
		return ClientOptions.builder()
				.disconnectedBehavior(ClientOptions.DisconnectedBehavior.REJECT_COMMANDS)
				.autoReconnect(true)
				.build();
		// @formatter:on
	}

	@Bean
    RedisStandaloneConfiguration redisStandaloneConfiguration() {
		log.debug("Initialize redis standalone configuration at {}:{}.",
				properties.getHost(), properties.getPort());
		RedisStandaloneConfiguration config = new RedisStandaloneConfiguration(
				properties.getHost(), properties.getPort());
		config.setDatabase(properties.getDatabase());
		config.setPassword(properties.getPassword());
		return config;
	}

	@Bean
	@ConditionalOnProperty({ "spring.redis.lettuce.pool.max-active" })
    LettucePoolingClientConfiguration lettucePoolConfig(ClientResources clientResources) {
		log.debug("Initialize lettuce pool configuration with custom properties.");
		GenericObjectPoolConfig<?> poolConfig = new GenericObjectPoolConfig<>();
		poolConfig.setMaxIdle(properties.getLettuce().getPool().getMaxIdle());
		poolConfig.setMinIdle(properties.getLettuce().getPool().getMinIdle());
		poolConfig.setMaxTotal(properties.getLettuce().getPool().getMaxActive());
		poolConfig.setMaxWaitMillis(
				properties.getLettuce().getPool().getMaxWait().toMillis());
		// @formatter:off
		return LettucePoolingClientConfiguration.builder()
				.poolConfig(poolConfig)
				.clientOptions(clientOptions())
				.clientResources(clientResources)
				.build();
		// @formatter:on
	}

	@Bean
	@ConditionalOnMissingBean
    LettucePoolingClientConfiguration simpleLettucePoolConfig(
			ClientResources clientResources) {
		log.debug("Initialize lettuce pool configuration using default.");
		// @formatter:off
		return LettucePoolingClientConfiguration.builder()
				.poolConfig(new GenericObjectPoolConfig<>())
				.clientOptions(clientOptions())
				.clientResources(clientResources)
				.build();
		// @formatter:on
	}

	@Bean
    RedisConnectionFactory redisConnectionFactory(
			LettucePoolingClientConfiguration lettucePoolConfig) {
		log.debug("Initialize lettuce connection factory.");
		return new LettuceConnectionFactory(redisStandaloneConfiguration(),
				lettucePoolConfig);
	}

}
