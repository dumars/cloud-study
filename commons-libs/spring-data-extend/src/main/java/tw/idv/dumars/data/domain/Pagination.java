package tw.idv.dumars.data.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.lang.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Pagination<T> implements Serializable {

	/**
	 * 目前頁數，即現在是在第幾頁.
	 */
	private int number;

	/**
	 * 分頁大小.
	 */
	private int size;

	/**
	 * 總頁數.
	 */
	private int totalPages;

	/**
	 * 本次查詢返回多少筆資料.
	 */
	private int numberOfElements;

	/**
	 * 總筆數.
	 */
	private long totalElements;

	/**
	 * 是否有上一頁.
	 */
	private boolean previousPage;

	/**
	 * 是否有下一頁.
	 */
	private boolean nextPage;

	/**
	 * 是否為第一頁.
	 */
	private boolean first;

	/**
	 * 是否為最後一頁.
	 */
	private boolean last;

	/**
	 * 本次查詢結果集合.
	 */
	private List<T> content;

	/**
	 * Default constructor.
	 */
	public Pagination() {
		this.content = new ArrayList<>();
	}

	/**
	 * 對應 {@Link PageInfo} 欄位.
	 * @param list 本次查詢結果集合.
	 */
	public void setList(@NonNull final List<T> list) {
		this.content = list;
	}

	/**
	 * 對應 {@Link PageInfo} 欄位.
	 * @param total 總筆數.
	 */
	public void setTotal(final long total) {
		this.totalElements = total;
	}

	/**
	 * 對應 {@Link PageInfo} 欄位.
	 * @param pageNum 目前頁數，即現在是在第幾頁.
	 */
	public void setPageNum(final int pageNum) {
		this.number = pageNum;
	}

	/**
	 * 對應 {@Link PageInfo} 欄位.
	 * @param pageSize 分頁大小.
	 */
	public void setPageSize(final int pageSize) {
		this.size = pageSize;
	}

	/**
	 * 對應 {@Link PageInfo} 欄位.
	 * @param pages 總頁數.
	 */
	public void setPages(final int pages) {
		this.totalPages = pages;
	}

	/**
	 * 對應 {@Link PageInfo} 欄位.
	 * @param firstPage 是否為第一頁.
	 */
	public void setFirstPage(final boolean firstPage) {
		this.first = firstPage;
	}

	/**
	 * 對應 {@Link PageInfo} 欄位.
	 * @param lastPage 是否為最後一頁.
	 */
	public void setLastPage(final boolean lastPage) {
		this.last = lastPage;
	}

	/**
	 * 對應 {@Link PageInfo} 欄位.
	 * @param hasPreviousPage 是否有上一頁.
	 */
	public void setHasPreviousPage(final boolean hasPreviousPage) {
		this.previousPage = hasPreviousPage;
	}

	/**
	 * 對應 {@Link PageInfo} 欄位.
	 * @param hasNextPage 是否有下一頁.
	 */
	public void setHasNextPage(final boolean hasNextPage) {
		this.nextPage = hasNextPage;
	}

	/**
	 * @return PageImpl
	 */
	public PageImpl<T> toPageImpl() {
		return new PageImpl<T>(getContent(), PageRequest.of(getNumber(), getSize()), getTotalElements());
	}

}
