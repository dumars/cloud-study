package tw.idv.dumars.feign.config;

import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import com.netflix.hystrix.strategy.concurrency.HystrixRequestVariableDefault;
import feign.Feign;
import feign.RequestInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

import javax.servlet.*;
import java.io.IOException;

@Slf4j
@Configuration
@ConditionalOnClass({Feign.class, WebSecurityConfigurerAdapter.class, SecurityContextHolder.class})
public class HystrixCredentialsContext {

    private static final HystrixRequestVariableDefault<Authentication> authentication = new HystrixRequestVariableDefault<>();

    public static HystrixRequestVariableDefault<Authentication> getInstance() {
        return authentication;
    }

    @Bean
    RequestInterceptor requestTokenBearerInterceptor() {
        return requestTemplate -> {
            Authentication auth = HystrixCredentialsContext.getInstance().get();

            if (auth != null) {
                log.debug( "try to forward the authentication by Hystrix, the Authentication Object: " + auth);
                requestTemplate.header("Authorization",
                        "bearer " + ((OAuth2AuthenticationDetails) auth.getDetails()).getTokenValue());
            } else {
                log.debug("attention, there is no Authentication Object needs to forward");
            }
        };
    }

    @Bean
    FilterRegistrationBean<Filter> hystrixFilter() {
        FilterRegistrationBean<Filter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(new Filter() {

            @Override
            public void init(FilterConfig filterConfig) throws ServletException {
            }

            @Override
            public void doFilter(ServletRequest request, ServletResponse response,
                                 FilterChain chain) throws IOException, ServletException {
                HystrixRequestContext.initializeContext();
                SecurityContext securityContext = SecurityContextHolder.getContext();

                if (securityContext != null) {
                    Authentication auth = securityContext.getAuthentication();
                    HystrixCredentialsContext.getInstance().set(auth);
                    log.trace(
                            "try to register the authentication into Hystrix Context, the Authentication Object: "
                                    + auth);
                }
                chain.doFilter(request, response);
            }

            @Override
            public void destroy() {
            }
        });

        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }

}
