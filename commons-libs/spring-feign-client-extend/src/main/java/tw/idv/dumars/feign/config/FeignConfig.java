package tw.idv.dumars.feign.config;

import feign.Feign;
import lombok.extern.slf4j.Slf4j;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Slf4j
@Configuration
@ConditionalOnClass(Feign.class)
@AutoConfigureBefore(FeignAutoConfiguration.class)
public class FeignConfig {

    @Bean
    @ConditionalOnMissingBean
    ConnectionPool connectionPool() {
        log.debug("Initialize okhttp3 connection pool for feign.");
        return new ConnectionPool(100, 5L, TimeUnit.MINUTES);
    }

    @Bean
    @ConditionalOnMissingBean
    OkHttpClient okHttpClient(ConnectionPool connectionPool) {
        log.debug("Initialize okhttp3 client for feign.");
        // @formatter:off
        return new OkHttpClient.Builder()
                .readTimeout(2, TimeUnit.MINUTES)
                .connectTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(4, TimeUnit.MINUTES)
                .followRedirects(true)
                .retryOnConnectionFailure(true)
                .connectionPool(connectionPool)
                .build();
        // @formatter:on
    }

}