package tw.idv.dumars.item.repository.data;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import tw.idv.dumars.item.model.Item;

@Repository
public interface ItemRepository extends PagingAndSortingRepository<Item, Long> {
}
