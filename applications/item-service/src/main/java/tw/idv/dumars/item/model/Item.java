package tw.idv.dumars.item.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Item {

    @Id
    @SequenceGenerator(name = "item_id_seq", sequenceName = "item_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "item_id_seq")
    @Column(name = "id", updatable = false)
    private Long id;
    @Column(nullable = false, length = 200)
    private String name;
    @Column(nullable = false)
    private Integer price;
}
