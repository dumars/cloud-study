package tw.idv.dumars.item.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import tw.idv.dumars.item.model.Item;
import tw.idv.dumars.item.repository.data.ItemRepository;

@Slf4j
@RestController
public class ItemController {

    @Autowired
    private ItemRepository itemRepository;

    @GetMapping("/items")
    public Page<Item> list(@PageableDefault(sort = { "id" }) Pageable pageable) {
        log.debug("Item list parameters, page: {}, size: {}", pageable.getPageNumber(), pageable.getPageSize());
        return itemRepository.findAll(pageable);
    }

    @GetMapping("/item/{id}")
    public ResponseEntity<Item> findById(@PathVariable("id") Long id) {
        return itemRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.noContent().build());
    }
}
