package tw.idv.dumars.item.loader;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import tw.idv.dumars.item.model.Item;
import tw.idv.dumars.item.repository.data.ItemRepository;

import java.util.stream.IntStream;

@Slf4j
@Component
public class InitialItemsLoader implements CommandLineRunner {

    @Autowired
    private ItemRepository itemRepository;

    @Override
    public void run(String... args) throws Exception {
        if (itemRepository.count() == 0) {
            log.debug("Create new item's sample data.");
            IntStream.range(1, 31).forEach(i -> {
                Item item = new Item();
                item.setName("Item-" + String.format("%03d", i));
                item.setPrice(RandomUtils.nextInt(300, 3000));
                itemRepository.save(item);
            });
        }
    }
}
