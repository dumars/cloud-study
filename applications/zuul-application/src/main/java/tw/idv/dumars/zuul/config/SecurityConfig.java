package tw.idv.dumars.zuul.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.session.Session;
import org.springframework.session.security.SpringSessionBackedSessionRegistry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

@Slf4j
@Profile("!test")
@Configuration
@EnableOAuth2Sso
@Order(value = 0)
public class SecurityConfig<S extends Session> extends WebSecurityConfigurerAdapter {

    @Value("${security.oauth2.client.user-logout-uri}")
    private String uri;
    @Autowired
    private OAuth2ProtectedResourceDetails resource;

    @Autowired
    private FindByIndexNameSessionRepository<S> sessionRepository;

    @Bean
    SpringSessionBackedSessionRegistry<S> sessionRegistry() {
        return new SpringSessionBackedSessionRegistry<>(this.sessionRepository);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        log.debug("Initialize http security configuration.");
        // @formatter:off
        http.csrf().disable()
            .formLogin().failureUrl("/login?error").permitAll()
                .and()
            .authorizeRequests()
                .antMatchers("/", "/auth/**", "/login**").permitAll()
                .anyRequest().authenticated()
                .and()
            .logout().invalidateHttpSession(true).clearAuthentication(true)
                .addLogoutHandler(new CustomLogoutHandler()).logoutSuccessUrl("/");

        // for session data redis
        http.sessionManagement()
                .maximumSessions(2)
                .maxSessionsPreventsLogin(false)
                .expiredUrl("/login?expired")
                .sessionRegistry(sessionRegistry());
        // @formatter:on
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        log.debug("Setup ignore web security matchers.");
        web.ignoring().antMatchers("/webjars/**", "/css/**", "/js/**", "/images/**", "/webfonts/**", "/actuator/**");
    }

    class CustomLogoutHandler implements LogoutHandler {

        @Override
        public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
            log.debug("logout zuul system.");
            if (authentication instanceof OAuth2Authentication) {
                OAuth2Authentication oauth2Authentication = (OAuth2Authentication) authentication;
                OAuth2RestTemplate template = tokenRelayTemplate(oauth2Authentication);
                template.delete(uri, new HashMap<>());
            }
        }

        private OAuth2RestTemplate tokenRelayTemplate(OAuth2Authentication authentication) {
            OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
            OAuth2ClientContext context = new DefaultOAuth2ClientContext(new DefaultOAuth2AccessToken(details.getTokenValue()));
            return new OAuth2RestTemplate(resource, context);
        }
    }
}
