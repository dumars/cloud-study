package tw.idv.dumars.zuul.config;

import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.concurrent.TimeUnit;

@Configuration
public class ZuulConfig {

    @Primary
    @Bean(name = "zuul.CONFIGURATION_PROPERTIES")
    @RefreshScope
    @ConfigurationProperties("zuul")
    public ZuulProperties zuulProperties() {
        return new ZuulProperties();
    }

    @Bean
    ConnectionPool connectionPool() {
        return new ConnectionPool(100, 5L, TimeUnit.MINUTES);
    }

    @Bean
    OkHttpClient okHttpClient(ConnectionPool connectionPool) {
        // @formatter:off
        return new OkHttpClient.Builder()
                .readTimeout(2, TimeUnit.MINUTES)
                .connectTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(4, TimeUnit.MINUTES)
                .followRedirects(true)
                .retryOnConnectionFailure(true)
                .connectionPool(connectionPool)
                .build();
        // @formatter:on
    }
}

