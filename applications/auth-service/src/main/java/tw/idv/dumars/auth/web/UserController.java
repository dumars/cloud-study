package tw.idv.dumars.auth.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@Slf4j
@RestController
public class UserController {

    @RequestMapping("/user/me")
    public Principal user(Principal principal) {
        return principal;
    }
}
