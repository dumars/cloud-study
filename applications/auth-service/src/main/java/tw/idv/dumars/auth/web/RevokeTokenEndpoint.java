package tw.idv.dumars.auth.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.provider.endpoint.FrameworkEndpoint;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import static org.apache.commons.lang.StringUtils.isNotEmpty;

@Slf4j
@FrameworkEndpoint
public class RevokeTokenEndpoint {

    @Autowired
    @Qualifier("consumerTokenServices")
    private ConsumerTokenServices consumerTokenServices;

    @RequestMapping(method = RequestMethod.DELETE, value = "/oauth/logout")
    @ResponseBody
    public void revokeToken(HttpServletRequest request) throws ServletException {
        String authorization = request.getHeader("Authorization");
        if (isNotEmpty(authorization) && authorization.contains("Bearer")) {
            String tokenId = authorization.substring("Bearer".length() + 1);
            log.debug("logout process, revoke token id: {}", tokenId);
            consumerTokenServices.revokeToken(tokenId);
        }
    }
}
