package tw.idv.dumars.auth.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "role")
public class Role implements GrantedAuthority, Serializable {
    @Id
    @SequenceGenerator(name = "role_id_seq", sequenceName = "role_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_id_seq")
    @Column(name = "role_id", updatable = false)
    private Long roleId;
    @Column(length = 40, unique = true, nullable = false)
    private String role;
    @Column(length = 100)
    private String label;

    @JsonIgnore
    @Override
    public String getAuthority() {
        return this.role;
    }
}
