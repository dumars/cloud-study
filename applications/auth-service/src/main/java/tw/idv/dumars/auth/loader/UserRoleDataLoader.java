package tw.idv.dumars.auth.loader;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import tw.idv.dumars.auth.model.Role;
import tw.idv.dumars.auth.model.User;
import tw.idv.dumars.auth.repository.data.RoleRepository;
import tw.idv.dumars.auth.repository.data.UserRepository;

import java.util.List;

@Slf4j
@Component
public class UserRoleDataLoader implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final String ADMIN = "admin";
    private static final String DUMARS = "dumars";

    @Override
    public void run(String... args) throws Exception {
        log.debug("Execute users and roles data creation.");
        userRepository.findByUsername(ADMIN)
                .ifPresentOrElse(admin -> {log.debug("Admin user exist, do nothing.");}, this::createAdmin);
        userRepository.findByUsername(DUMARS)
                .ifPresentOrElse(admin -> {log.debug("Dumars user exist, do nothing.");}, this::createDumars);
    }

    private void createAdmin() {
        Role admin = new Role();
        admin.setRole("ROLE_ADMIN");
        admin.setLabel("Administrator");

        admin = roleRepository.save(admin);
        log.debug("Role: {}", admin);

        List<Role> roles = Lists.newArrayList(admin);

        User user = new User();
        user.setUsername(ADMIN);
        user.setPassword(passwordEncoder.encode("admin"));
        user.setName("Administrator");
        user.setEmail("admin@test.com");
        user.setRoles(roles);

        userRepository.save(user);
    }

    private void createDumars() {
        Role simpleUser = new Role();
        simpleUser.setRole("USER");
        simpleUser.setLabel("Simple User");
        simpleUser = roleRepository.save(simpleUser);
        List<Role> roles = Lists.newArrayList(simpleUser);

        User dumars = new User();
        dumars.setUsername(DUMARS);
        dumars.setPassword(passwordEncoder.encode("123456"));
        dumars.setName("Dumars");
        dumars.setEmail("dumars@test.com");
        dumars.setRoles(roles);

        userRepository.save(dumars);
    }
}
