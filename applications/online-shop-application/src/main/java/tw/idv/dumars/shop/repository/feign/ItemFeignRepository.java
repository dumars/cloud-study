package tw.idv.dumars.shop.repository.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tw.idv.dumars.data.domain.Pagination;
import tw.idv.dumars.shop.dto.Item;

@FeignClient(value = "ITEM-SERVICE")
public interface ItemFeignRepository {

    @RequestMapping(method = RequestMethod.GET,  value = "/items", consumes = MediaType.APPLICATION_JSON_VALUE)
    Pagination<Item> findAll(Pageable pageable);

    @RequestMapping(method = RequestMethod.GET, value = "/item/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    Item findById(@PathVariable("id") Long id);
}
