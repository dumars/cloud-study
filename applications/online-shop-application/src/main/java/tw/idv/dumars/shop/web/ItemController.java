package tw.idv.dumars.shop.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import tw.idv.dumars.shop.dto.Item;
import tw.idv.dumars.shop.repository.feign.ItemFeignRepository;

@Slf4j
@Controller
public class ItemController {

    @Autowired
    private ItemFeignRepository itemFeignRepository;

    @GetMapping("/items")
    public String list(@PageableDefault Pageable pageable, Model model) {
        log.debug("Item list parameters, page: {}, size: {}", pageable.getPageNumber(), pageable.getPageSize());
        model.addAttribute("page", itemFeignRepository.findAll(pageable));
        return "item/list";
    }

    @ResponseBody
    @GetMapping("/item/{id}")
    public Item findItemById(@PathVariable("id") Long id) {
        return itemFeignRepository.findById(id);
    }
}
