package tw.idv.dumars.shop.dto;

import lombok.Data;

@Data
public class Item {

    private Long id;
    private String name;
    private Integer price;
}
